import React from 'react';

class SearchBar extends React.Component {
    state = { term: ''};

    onFormSubmit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state.term); 
    };

    render(){
        return (
         <div className="ui segment">
             <h3>Введите запрос ниже. <br />Пример: flowers, cars, apple</h3>
            <form onSubmit={this.onFormSubmit} className="ui form">
                <div className="field">
                    <label>Поиск изображения</label>
                    <input type="text" value={this.state.term} onChange={e => this.setState({ term: e.target.value} )} />
                </div>
            </form>
        </div>);
    }
}

export default SearchBar;